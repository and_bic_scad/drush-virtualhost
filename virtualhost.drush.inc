<?php

/**
 * Implements hook_drush_command().
 */
function virtualhost_drush_command() {
  $items['host-add'] = array(
    'description' => 'Add localhost to /etc/hosts file.',
    'callback' => 'drush_virtualhost_hosts_file_add',
    'arguments' => array(
      'hostname' => 'The hostname to add.',
    ),
    'options' => array(
      'local-ipv4' => 'The local IPv4 address to use. Default is 127.0.0.1.',
      'local-ipv6' => 'The local IPv6 address to use. Default is ::1.',
      'hosts-file' => 'The location of the hosts file. Default is /etc/hosts.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  $items['host-remove'] = array(
    'description' => 'Removes localhost record from /etc/hosts file.',
    'callback' => 'drush_virtualhost_hosts_file_remove',
    'arguments' => array(
      'hostname' => 'The hostname to remove.',
    ),
    'options' => array(
      'hosts-file' => 'The location of the hosts file. Default is /etc/hosts.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  $items['apache-site-list'] = array(
    'description' => 'Lists Apache site configuration files.',
    'callback' => 'drush_virtualhost_apache_site_list',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  $items['apache-site-add'] = array(
    'description' => 'Add an Apache site configuration file.',
    'callback' => 'drush_virtualhost_apache_site_add',
    'arguments' => array(
      'hostname' => 'The hostname to use.',
      'docroot' => 'The docroot to use.',
    ),
    'options' => array(
      'aliases' => 'A space-separated list of additional ServerAlias directives to use. Defaults to a wildcard *.$hostname to support subdomains.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  $items['apache-site-enable'] = array(
    'description' => 'Enables an Apache site configuration file.',
    'callback' => 'drush_virtualhost_apache_site_enable',
    'arguments' => array(
      'hostname' => 'The hostname to use.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  $items['apache-site-disable'] = array(
    'description' => 'Disables an Apache site configuration file.',
    'callback' => 'drush_virtualhost_apache_site_disable',
    'arguments' => array(
      'hostname' => 'The hostname to use.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  $items['apache-site-remove'] = array(
    'description' => 'Removes an Apache site configuration file.',
    'callback' => 'drush_virtualhost_apache_site_remove',
    'arguments' => array(
      'hostname' => 'The site hostname.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  $items['apache-restart'] = array(
    'description' => 'Restarts the Apache server.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  return $items;
}

function _drush_virtualhost_get_site_file($hostname) {
  $version = virtualhost_apache_version();
  if (!$version) {
    drush_set_error('DRUSH_VIRTUALHOST_APACHE_NO_VERSION', dt('Unable to determine the local Apache server version using apachectl -v.'));
  }

  $file = '/etc/apache2/sites-available/' . $hostname;
  if (version_compare($version, '2.4', '>=')) {
    $file .= '.conf';
  }
  return $file;
}

function _drush_virtualhost_get_site_file_contents(array $variables) {
  $contents = file_get_contents(dirname(__FILE__) . '/virtualhost.template');

  $variables['date'] = date("Y-m-d");
  if (extension_loaded('posix')) {
    $process = posix_getpwuid(posix_geteuid());
    $variables['user'] = $process['name'];
  }
  else {
    $variables['user'] = get_current_user();
  }

  // Replace variables in a Twig/Liquid syntax.
  foreach ($variables as $key => $value) {
    $contents = str_replace('{{ ' . $key . ' }}', $value, $contents);
  }

  // Replace any empty variables.
  $contents = preg_replace('/\{\{.*\}\}/', '', $contents);

  return $contents;
}

function drush_virtualhost_apache_site_add_validate($hostname, $docroot) {
  if (!is_dir($docroot)) {
    return drush_set_error('DRUSH_VIRTUALHOST_DOCROOT_NOT_EXISTS', dt('The docroot at @dir does not exist.', array('@dir' => $docroot)));
  }

  switch (PHP_OS) {
    case 'Darwin':
      // The /var/www directory doesn't exist on Macs.
      if (!is_dir('/var/www')) {
        drush_op_system("sudo mkdir -p /var/www");
      }
      break;
  }
}

function drush_virtualhost_apache_site_info($file) {
  $site = array();
  if ($contents = file_get_contents($file)) {
    $site['file'] = basename($file);
    $enabled = str_replace('sites-available', 'sites-enabled', $file);
    $site['enabled'] = is_file($enabled) ? dt('Yes') : dt('No');
    if (preg_match('/(?<!\#)ServerName (.+)/', $contents, $matches)) {
      $site['ServerName'] = $matches[1];
    }
    else {
      $site['ServerName'] = dt('Unknown');
    }
    if (preg_match_all('/(?<!\#)ServerAlias (.+)/', $contents, $matches)) {
      $site['ServerAlias'] = trim(implode(' ', $matches[1]));
    }
    else {
      $site['ServerAlias'] = '';
    }
    if (preg_match('/(?<!\#)DocumentRoot (.+)/', $contents, $matches)) {
      $site['DocumentRoot'] = $matches[1];
    }
    else {
      $site['DocumentRoot'] = dt('Unknown');
    }
  }
  return $site;
}

function drush_virtualhost_apache_site_list() {
  $dir = dirname(_drush_virtualhost_get_site_file('dummy'));
  $files = array_diff(scandir($dir), array('.', '..'));
  $sites = array();
  $sites[] = array(
    dt('File'),
    dt('Enabled?'),
    dt('ServerName'),
    dt('ServerAlias'),
    dt('DocumentRoot'),
  );
  foreach ($files as $file) {
    if ($site = drush_virtualhost_apache_site_info($dir . '/' . $file)) {
      $sites[] = $site;
    }
    else {
      $sites[] = array(
        $dir . '/' . $file,
        dt('UNREADABLE'),
        '',
        '',
        '',
      );
    }
  }
  return drush_print_table($sites, TRUE);
}

function drush_virtualhost_site_create($hostname, $docroot) {
  drush_invoke('apache-site-create', array($hostname, $docroot));
  drush_invoke('host-add', array($hostname));
  if ($aliases = drush_get_option('aliases')) {
    $aliases = explode(' ', $aliases);
    foreach ($aliases as $alias) {
      if (strpos('*', $alias) !== FALSE) {
        drush_invoke('host-add', array($alias));
      }
    }
  }
}

function drush_virtualhost_apache_site_add($hostname, $docroot) {
  $file = _drush_virtualhost_get_site_file($hostname);
  $aliases = drush_get_option('aliases', "*.{$hostname}");
  $contents = _drush_virtualhost_get_site_file_contents(array(
    'hostname' => $hostname,
    'docroot' => '/var/www/' . $hostname,
    'logdir' => '/var/log/apache2',
    'aliases' => $aliases,
  ));
  drush_op_system("printf '%s' '{$contents}' | sudo tee {$file}");
  drush_op_system("sudo rm /var/www/{$hostname}");
  drush_op_system("sudo ln -sfnv {$docroot} /var/www/{$hostname}");

  drush_invoke('host-add', array($hostname));
  if (!empty($aliases)) {
    foreach (explode(' ', $aliases) as $alias) {
      if (strpos('*', $alias) !== FALSE) {
        drush_invoke('host-add', array($alias));
      }
    }
  }

  drush_invoke('apache-site-enable', array($hostname));
  drush_invoke('apache-restart');
}

function drush_virtualhost_apache_site_enable($hostname) {
  switch (PHP_OS) {
    case 'Linux':
      drush_op_system("sudo a2ensite {$hostname}");
      break;

    case 'Darwin':
      $file = _drush_virtualhost_get_site_file($hostname);
      $enabled = str_replace('sites-available', 'sites-enabled', $file);
      drush_op_system("sudo ln -sfnv {$file} {$enabled}");
      break;
  }
}

function drush_virtualhost_apache_site_disable($hostname) {
  switch (PHP_OS) {
    case 'Linux':
      drush_op_system("sudo a2dissite {$hostname}");
      break;

    case 'Darwin':
      $file = _drush_virtualhost_get_site_file($hostname);
      $enabled = str_replace('sites-available', 'sites-enabled', $file);
      drush_op_system("sudo rm -v {$enabled}");
      break;
  }
}

function drush_virtualhost_apache_site_remove($hostname) {
  $file = _drush_virtualhost_get_site_file($hostname);
  $info = drush_virtualhost_apache_site_info($file);

  drush_invoke('apache-site-disable', array($hostname));
  drush_op_system("sudo rm /var/www/{$hostname}");
  drush_op_system("sudo rm {$file}");

  drush_invoke('host-remove', array($hostname));
  if (!empty($info['ServerAlias'])) {
    foreach (explode(' ', $info['ServerAlias']) as $alias) {
      if (strpos('*', $alias) !== FALSE) {
        drush_invoke('host-remove', array($alias));
      }
    }
  }

  drush_invoke('apache-restart');
}

function drush_virtualhost_apache_restart() {
  switch (PHP_OS) {
    case 'Linux':
      drush_op_system("sudo service apache2 restart");
      break;

    case 'Darwin':
      drush_op_system("sudo apachectl restart");
      break;
  }
}

function drush_virtualhost_hosts_file_add($hostname) {
  $ipv4 = drush_get_option('local-ipv4', '127.0.0.1');
  $ipv6 = drush_get_option('local-ipv6', '::1');
  $hostsfile = drush_get_option('hosts-file', '/etc/hosts');
  $params = array(
    '@hostname' => $hostname,
    '@hostsfile' => $hostsfile,
    '@ipv4' => $ipv4,
    '@ipv6' => $ipv6,
  );

  if (!is_file($hostsfile) || !is_readable($hostsfile)) {
    return drush_set_error('DRUSH_LOCAL_HOSTS_FILE_UNREADABLE', dt("Hosts file @hosts not found.", $params));
  }

  if (!empty($ipv4)) {
    if (_drush_virtualhost_host_file_record_exists($ipv4, $hostname, $hostsfile)) {
      drush_set_error('DRUSH_LOCAL_HOSTS_FILE_RECORD_EXISTS', dt("Hostname record @ipv4 @hostname already exists in the hosts file @hostsfile.", $params));
    }
    else {
      _drush_virtualhost_host_file_record_add($ipv4, $hostname, $hostsfile);
      drush_op_system("sudo chmod 644 {$hostsfile}");
      drush_log(dt("Added hostname record '@ipv4 @hostname' to @hostsfile.", $params), 'ok');
    }
  }
  if (!empty($ipv6)) {
    if (_drush_virtualhost_host_file_record_exists($ipv6, $hostname, $hostsfile)) {
      drush_set_error('DRUSH_LOCAL_HOSTS_FILE_RECORD_EXISTS', dt("Hostname record @ipv6 @hostname already exists in the hosts file @hostsfile.", $params));
    }
    else {
      _drush_virtualhost_host_file_record_add($ipv6, $hostname, $hostsfile);
      drush_op_system("sudo chmod 644 {$hostsfile}");
      drush_log(dt("Added hostname record '@ipv6 @hostname' to @hostsfile.", $params), 'ok');
    }
  }
}

function drush_virtualhost_hosts_file_remove($hostname) {
  $ipv4 = drush_get_option('local-ipv4', '127.0.0.1');
  $ipv6 = drush_get_option('local-ipv6', '::1');
  $hostsfile = drush_get_option('hosts-file', '/etc/hosts');
  $params = array(
    '@hostname' => $hostname,
    '@hostsfile' => $hostsfile,
    '@ipv4' => $ipv4,
    '@ipv6' => $ipv6,
  );

  if (!is_file($hostsfile) || !is_readable($hostsfile)) {
    return drush_set_error('DRUSH_LOCAL_HOSTS_FILE_UNREADABLE', dt("Hosts file @hostsfile not found.", $params));
  }

  $tempfile = drush_tempnam('hosts_');
  drush_op_system("cp {$hostsfile} {$tempfile}");

  if (!empty($ipv4)) {
    if (_drush_virtualhost_host_file_record_remove($ipv4, $hostname, $tempfile)) {
      drush_log(dt("Removed hostname record '@ipv4 @hostname' from @hostsfile.", $params), 'ok');
      drush_op_system("sudo cp {$tempfile} {$hostsfile}");
      drush_op_system("sudo chmod 644 {$hostsfile}");
    }
    else {
      drush_set_error('DRUSH_LOCAL_HOSTS_FILE_RECORD_NOT_EXISTS', dt("Hostname record @ipv4 @hostname does not exist in the hosts file @hostsfile.", $params));
    }
  }
  if (!empty($ipv6)) {
    if (_drush_virtualhost_host_file_record_remove($ipv6, $hostname, $tempfile)) {
      drush_log(dt("Removed hostname record '@ipv6 @hostname' from @hostsfile.", $params), 'ok');
      drush_op_system("sudo cp {$tempfile} {$hostsfile}");
      drush_op_system("sudo chmod 644 {$hostsfile}");
    }
    else {
      drush_set_error('DRUSH_LOCAL_HOSTS_FILE_RECORD_NOT_EXISTS', dt("Hostname record @ipv6 @hostname does not exist in the hosts file @hostsfile.", $params));
    }
  }

  //echo file_get_contents($tempfile);
}

function _drush_virtualhost_host_file_record_exists($address, $hostname, $hostsfile) {
  $contents = file_get_contents($hostsfile);
  return preg_match('/^' . preg_quote($address) . '\s+.*\b' . preg_quote($hostname) . '\b\s*$/m', $contents);
}

function _drush_virtualhost_host_file_record_add($address, $hostname, $hostsfile) {
  drush_op_system("printf '%s\t%s\n' {$address} {$hostname} | sudo tee -a {$hostsfile} > /dev/null");
}

function _drush_virtualhost_host_file_record_remove($address, $hostname, $hostsfile) {
  $modified = FALSE;
  $contents = file_get_contents($hostsfile);
  $lines = preg_split('/\n|\r|\n\r|\r\n/', $contents);
  foreach ($lines as $index => $line) {
    if (preg_match('/^(' . preg_quote($address) . '\s+.*)\b' . preg_quote($hostname) . '\s*\b/', $line)) {
      $modified = TRUE;
      $line = preg_replace('/^(' . preg_quote($address) . '\s+.*)\b' . preg_quote($hostname) . '\s*\b(.*)$/', '$1$2', $line);
      if (preg_match('/^' . preg_quote($address) . '\s+$/', $line)) {
        unset($lines[$index]);
      }
      else {
        $lines[$index] = $line;
      }
    }
  }
  if ($modified) {
    $contents = implode(PHP_EOL, $lines);
    $result = file_put_contents($hostsfile, $contents);
  }
  return $modified;
}

function virtualhost_apache_version() {
  static $version;

  if (!isset($version)) {
    $cid = drush_get_cid('apache-version');
    if ($cache = drush_cache_get($cid)) {
      $version = $cache->data;
    }
    else {
      $version = FALSE;
      if (drush_shell_exec('apachectl -v')) {
        $output = drush_shell_exec_output();
        foreach ($output as $line) {
          if (preg_match('/^Server version: Apache\/([\d\.]+)/', $line, $matches)) {
            $version = $matches[1];
            drush_cache_set($cid, $version);
          }
        }
      }
    }
  }

  return $version;
}
